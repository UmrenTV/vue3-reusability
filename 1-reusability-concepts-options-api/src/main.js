import { createApp } from 'vue';
import globalLogger from './mixins/global-logger.js'

import App from './App.vue';

const app = createApp(App)
app.mixin(globalLogger); // This registers and adds this to every component you are going to make.
// which  means that every time you load a component that mounted(){} stuff will run.
// I guess this can be useful if you do something like "route.params" event, so you have like
// analytics running in the background, collecting mounted, unmounted clicks, so you know
// where your users are clicking, how long they are staying, what they are interacting with, etc.
// That is actually quite clever, if you want to build your own database of your users interactions
// using something like mounted, unmounted events and tracking the time between those events, and the
// interaction in between with other components on the page, if you can use component path, placement,
// route, or something else in the mixin, as dyniamic value.

app.mount('#app');
