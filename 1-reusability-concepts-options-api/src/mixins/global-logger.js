export default {
mounted() {
console.log('The page and logger has been loaded.')
}
} // Global mixins we usually import in main.js btw.

// This is example for a global mixin. It is generally never used this kind of practice,
// except for some analytics, or login logic, or something that requires to add it to
// most, if not all, the components you going to make. Or pages, or something.
//
// Another Note: if you have mounted in a mixin, or computed, or watch, or something,
// and you use it in a component that also has some of those shenenigans, then both
// of those will get executed, with the component stuff running last, so it overrides
// stuffs from the mixin, if there is need for it. Same as data properties.
//
// If you have data properties defined in data() in mixin that you are calling, and in
// the component itself, as we can see in this project, the component data "wins".
// And why is that? Because the component data is getting executed last, so it overrides
// the mixin data with it's own. THIS IS ONLY IF THEY HAVE THE SAME NAME, ofcourse.
