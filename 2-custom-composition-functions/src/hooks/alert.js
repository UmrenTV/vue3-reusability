import { ref } from 'vue';

//export default function useAlert() {
export default function useAlert(startingVisibility = false) {
    //const alertIsVisible = ref(false);
    const alertIsVisible = ref(startingVisibility);

    // We can also receive arguments in our custom hooks, as well as set their default value (which is modern
    // javascript syntax) like above with startingVisibility. We can use as many arguments we need.

    function showAlert() {
        alertIsVisible.value = true;
    }
    function hideAlert() {
        alertIsVisible.value = false;
    }

    return [alertIsVisible, showAlert, hideAlert];
    // A note on returning values in composition API: You can also return all
    // these values as array, it doesn't have to be an object since this is
    // actually our function. So we can put all the values our function returns
    // in an array, and use array destructuring (you'll see that in components)
    // to return these values to the template we're going to be using them on.
    //
    // We don't have to use array, we can also use object and use object destructuring
    // in the same way, but for the sake of this point, lets use array.
}

// It's not a MUST, but common convention when creating hooks, is to name them starting with use.
// It's logical that way, and is also convenient, since it allows you to use some other  code.
//
// THE BIG DIFFERENCE between hooks and mixins is that now you can clearly see, no matter the project
// where the constants are being used from, which hook gives you which variables as return values, and
// where the template is using the stuffs from, unlike with mixins with Options API
