import { ref, watch, computed } from 'vue';

export default function useSearch(items, searchProp) {
    const enteredSearchTerm = ref('');
    const activeSearchTerm = ref('');

    const availableItems = computed(function() {
        let filteredItems = [];
        if (activeSearchTerm.value) {
            filteredItems = items.value.filter((item) =>
                //   usr.fullName.includes(activeSearchTerm.value)
                // instead of using usr or prj which is quite specific name for a hook that should be
                // used more globally, we can just use general name and instead of searching based on the
                // fullName or projectName or something, we can leave that to be passed as argument
                // in our hook, as searchProp. So if we pass "fullName" as searchProp, it will go into
                // it will search for that searchProp name inside of item, pretty much like item.fullName
                // It is object reading way, by putting the key name in the square brackets, and getting the value in return (if the key is found)
                item[searchProp].includes(activeSearchTerm.value)
            );
        } else if (items.value) {
            filteredItems = items.value;
        }
        return filteredItems;
    });

    watch(enteredSearchTerm, function(newValue) {
        setTimeout(() => {
            if (newValue === enteredSearchTerm.value) {
                activeSearchTerm.value = newValue;
            }
        }, 300);
    });

    function updateSearch(val) {
        enteredSearchTerm.value = val;
    }
    return {
        enteredSearchTerm,
        // activeSearchTerm - This we won't return because it's not used in the template, or anywhere else, except in the hook.
        availableItems,
        updateSearch,
    };
}

// Another way of solving the custom search function is if we set it up to receive the function
// as argument to our  hook. useSearch(items, searchFn) or soemthing. Then we can do
// filteredItems = searchFn . We just have to set that up beforehand in our component,
// by doing something like:
// searchFn = items.filter((item) => item.fullName.includes(activeSearchTerm.value)
